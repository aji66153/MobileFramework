import 'package:flutter/material.dart';

void main() => runApp(BelajarImage());

class BelajarImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Center(child: Text("BelajarFlutter.com")),
      ),
      body: Column(
        children: <Widget>[
          Image.asset('assets/img/monkey.jpg'),
          Text(
            'Hallo Whatsup Brow',
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              color: Colors.blue,
            ),
          ),
        ],
      ),
    ));
  }
}
