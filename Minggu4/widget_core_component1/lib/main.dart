import 'package:flutter/material.dart';

void main() {
  runApp(MyHomePage());
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Aplikasi pertama'),
          backgroundColor: Colors.purple,
        ),

        // kolom dan baris
        // body: MaterialButton(
        //   child:  Text('Button'),
        //   onPressed: () {},
        // ),
        // body: Column(
        //   mainAxisAlignment: MainAxisAlignment.start, // vertikal
        //   crossAxisAlignment: CrossAxisAlignment.start, // horizontal
        //   children: <Widget>[
        //     Text('1'),
        //     Text('2'),
        //     Text('3'),
        //     Text('4'),
        //     Text('5'),
        //     Row(
        //       children:  <Widget>[
        //         Text('1'),
        //         Text('2'),
        //         Text('3'),
        //         Text('4'),
        //         Text('5'),
        //       ],
        //     ),
        //   ],
        // )

        // icon
        // body: Container(
        //     padding:  EdgeInsets.all(20),
        //     child: Row(
        //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //       children: [
        //         Column(
        //           children:  <Widget>[
        //             Icon(Icons.home),
        //             Text('Home'),
        //           ],
        //         ),
        //         Column(
        //           children:  <Widget>[
        //             Icon(Icons.email),
        //             Text('Message'),
        //           ],
        //         ),
        //         Column(
        //           children:  <Widget>[
        //             Icon(Icons.account_circle),
        //             Text('Home'),
        //           ],
        //         )
        //       ],
        //     )),

        // container
        // body: Container(
        //   margin:  EdgeInsets.fromLTRB(20, 20, 0, 0),
        //   width: 100,
        //   height: 50,
        //   decoration: BoxDecoration(
        //     color: Colors.purple,
        //     borderRadius: BorderRadius.circular(10),
        //   ),
        //   child:  Center(
        //       child: Text(
        //     'Hello World',
        //     style: TextStyle(
        //       fontSize: 16,
        //       color: Colors.white,
        //     ),
        //   )),
        // ),

        // button
        // body: Column(
        //   children: <Widget>[
        //     RaisedButton(
        //       color: Colors.teal,
        //       onPressed: () {},
        //       child: Text('Raised Button'),
        //     ),
        //     MaterialButton(
        //         color: Colors.amber,
        //         onPressed: () {},
        //         child: Text('Material Button')),
        //     FlatButton(
        //         color: Colors.blue,
        //         onPressed: () {},
        //         child: Text('Flat Button')),
        //   ],
        // ),

        body: Padding(
          padding: EdgeInsets.all(10),
          child: Form(
              child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Nama',
                  hintText: 'Masukkan nama',
                  icon: Icon(Icons.person),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Email',
                  hintText: 'Masukkan email',
                  icon: Icon(Icons.email),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Password',
                  hintText: 'Masukkan password',
                  icon: Icon(Icons.lock),
                ),
              ),
              RaisedButton(
                color: Colors.blue,
                onPressed: () {},
                child: Text('Submit'),
              ),
            ],
          )),
        ),
      ),
    );
  }
}
